package com.chipcerio.pagingrxjava.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chipcerio.pagingrxjava.App
import com.chipcerio.pagingrxjava.R
import com.chipcerio.pagingrxjava.addTo
import com.chipcerio.pagingrxjava.di.PixabayUsersInjection
import com.chipcerio.pagingrxjava.model.Params
import com.chipcerio.pagingrxjava.model.Query
import com.chipcerio.pagingrxjava.printerr
import com.chipcerio.pagingrxjava.toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    
    private val disposables = CompositeDisposable()
    private lateinit var pixabayAdapter: PixabayAdapter
    private lateinit var viewModel: ViewModel
    
    private val usersStream = PublishSubject.create<Int>()
    private var page = 1
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewModel()
        setupRecyclerView()
        
        bindPagingStream()
        usersStream.onNext(page)
        
        // simulatePaging()
    }
    
    private fun simulatePaging() {
        val handler = Handler(Looper.getMainLooper())
        handler.post(object : Runnable {
            override fun run() {
                page++
                usersStream.onNext(page)
                if (page <= 5) {
                    handler.postDelayed(this, 2000)
                }
            }
        })
    }
    
    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }
    
    private fun bindPagingStream() {
        val query = Query(
            key = Params.KEY,
            lang = Params.LANG,
            imageType = Params.IMAGE_TYPE,
            category = Params.CATEGORY
        )
        
        usersStream.observeOn(Schedulers.io())
            .concatMap {
                viewModel.fetchUsers(it, query)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { toast("Page $page") }
            .subscribe(
                { pixabayAdapter.setItems(it) },
                { printerr("error in getting results") })
            .addTo(disposables)
    }
    
    private fun setupViewModel() {
        val service = (application as App).service()
        viewModel = ViewModel(PixabayUsersInjection.providesPixabayUsersSource(service))
    }
    
    private fun setupRecyclerView() {
        val verticalLayoutManager = LinearLayoutManager(this).apply {
            orientation = LinearLayoutManager.VERTICAL
        }
        pixabayAdapter = PixabayAdapter()
        recyclerView.apply {
            layoutManager = verticalLayoutManager
            adapter = pixabayAdapter
        }
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) and (newState == RecyclerView.SCROLL_STATE_IDLE)) {
                    println("end of list reached")
                }
            }
        })
    }
}
