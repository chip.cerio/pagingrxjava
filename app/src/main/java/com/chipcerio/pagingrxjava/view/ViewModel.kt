package com.chipcerio.pagingrxjava.view

import com.chipcerio.pagingrxjava.data.UsersSource
import com.chipcerio.pagingrxjava.model.Query
import io.reactivex.Observable

class ViewModel(private val repository: UsersSource) {
    
    fun fetchUsers(page: Int, query: Query): Observable<List<String>> {
        return repository.getUsers(page, query)
    }
}