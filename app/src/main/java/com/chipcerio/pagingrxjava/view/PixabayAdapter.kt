package com.chipcerio.pagingrxjava.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class PixabayAdapter : RecyclerView.Adapter<PixabayAdapter.ViewHolder>() {
    
    private var list: List<String> = emptyList()
    
    override fun getItemCount(): Int = list.size
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val root = inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
        return ViewHolder(root)
    }
    
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }
    
    fun setItems(items: List<String>) {
        val temp = list.toMutableList()
        temp.addAll(items)
        list = temp.toList()
        notifyDataSetChanged()
    }
    
    class ViewHolder(
        private val containerView: View
    ) : RecyclerView.ViewHolder(containerView) {
        
        private val textView: TextView = containerView.findViewById(android.R.id.text1)
        
        fun bind(item: String) {
            textView.text = item
        }
    }
}