package com.chipcerio.pagingrxjava

import android.app.Application
import com.chipcerio.pagingrxjava.service.PixabayService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class App : Application() {
    
    private lateinit var service: PixabayService
    
    override fun onCreate() {
        super.onCreate()
        service = retrofit().create(PixabayService::class.java)
    }
    
    fun service(): PixabayService = service
    
    private fun retrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://pixabay.com")
            .addConverterFactory(GsonConverterFactory.create(gson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okhttp())
            .build()
    }
    
    private fun gson(): Gson {
        return GsonBuilder().setLenient().create()
    }
    
    private fun okhttp(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor())
            .build()
    }
    
    private fun httpLoggingInterceptor(): Interceptor {
        return HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BASIC
        }
    }
}