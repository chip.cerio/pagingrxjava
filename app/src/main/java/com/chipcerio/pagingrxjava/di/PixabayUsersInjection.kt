package com.chipcerio.pagingrxjava.di

import com.chipcerio.pagingrxjava.data.PixabayUserRepository
import com.chipcerio.pagingrxjava.data.UsersSource
import com.chipcerio.pagingrxjava.service.PixabayService

class PixabayUsersInjection {
    
    companion object {
        fun providesPixabayUsersSource(api: PixabayService): UsersSource = PixabayUserRepository(api)
    }
}