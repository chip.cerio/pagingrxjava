package com.chipcerio.pagingrxjava.service

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface PixabayService {

    @GET("/api")
    fun getResults(
        @Query("key") key: String,
        @Query("lang") lang: String,
        @Query("image_type") imageType: String,
        @Query("category") category: String,
        @Query("page") page: Int
    ): Observable<PixabayResult>
}