package com.chipcerio.pagingrxjava.service

import com.chipcerio.pagingrxjava.model.Pixabay

data class PixabayResult(
    val total: Int = 0,
    val totalHits: Int = 0,
    val hits: List<Pixabay> = emptyList()
)
