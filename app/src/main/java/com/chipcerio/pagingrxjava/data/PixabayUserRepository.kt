package com.chipcerio.pagingrxjava.data

import com.chipcerio.pagingrxjava.model.Query
import com.chipcerio.pagingrxjava.service.PixabayService
import io.reactivex.Observable

class PixabayUserRepository(private val api: PixabayService) : UsersSource {
    
    override fun getUsers(page: Int, query: Query): Observable<List<String>> {
        return api.getResults(
            key = query.key,
            lang = query.lang,
            imageType = query.imageType,
            category = query.category,
            page = page)
            .map { res ->
                res.hits.map { it.user }
            }
    }
}
