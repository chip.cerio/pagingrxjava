package com.chipcerio.pagingrxjava.data

import com.chipcerio.pagingrxjava.model.Query
import io.reactivex.Observable

interface UsersSource {
    fun getUsers(page: Int, query: Query): Observable<List<String>>
}