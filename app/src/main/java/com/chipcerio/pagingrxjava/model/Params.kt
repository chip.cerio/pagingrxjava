package com.chipcerio.pagingrxjava.model

class Params {
    companion object {
        const val KEY = "16977777-d20890cf3e21433bb02e27ddc"
        const val LANG = "en"
        const val IMAGE_TYPE = "photo"
        const val CATEGORY = "people"
        const val PAGE = 0
    }
}