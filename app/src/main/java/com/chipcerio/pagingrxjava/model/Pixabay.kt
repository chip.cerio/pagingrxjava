package com.chipcerio.pagingrxjava.model

data class Pixabay(
    val id: Int = 5246529,
    val pageURL: String = "https://pixabay.com/photos/fantasy-angel-reflection-water-5246529/",
    val type: String = "photo",
    val tags: String = "fantasy, angel, reflection",
    val previewURL: String = "https://cdn.pixabay.com/photo/2020/06/01/12/58/fantasy-5246529_150.jpg",
    val previewWidth: Int = 150,
    val previewHeight: Int = 100,
    val webformatURL: String = "https://pixabay.com/get/53e2d1454f50a514f1dc8460962931771238d9e2544c704c7c2e7cdd9548c55a_640.jpg",
    val webformatWidth: Int = 640,
    val webformatHeight: Int = 427,
    val largeImageURL: String = "https://pixabay.com/get/53e2d1454f50a514f6da8c7dda7936781c38d9e254566c4870267ad39e4fc058ba_1280.jpg",
    val imageWidth: Int = 7087,
    val imageHeight: Int = 4724,
    val imageSize: Int = 4895821,
    val views: Int = 21381,
    val downloads: Int = 17190,
    val favorites: Int = 42,
    val likes: Int = 114,
    val comments: Int = 74,
    val user_id: Int = 3764790,
    val user: String = "enriquelopezgarre",
    val userImageURL: String = "https://cdn.pixabay.com/user/2020/06/03/11-05-03-625_250x250.jpg"
)