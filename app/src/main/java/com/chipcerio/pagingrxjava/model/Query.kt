package com.chipcerio.pagingrxjava.model

data class Query(
    val key: String = "",
    val lang: String = "",
    val imageType: String = "",
    val category: String = ""
)